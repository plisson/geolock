const r = 6378.1370 // m
const pi = Math.PI
console.log(r, pi)
// const compassDisc = document.getElementById('compassDiscImg')

function coordcirctocart(m) {
    var teta = m[0]
    teta = teta * pi / 180
    var phi = m[1]
    phi = phi * pi / 180
    var x = r * Math.cos(phi) * Math.cos(teta)
    var y = r * Math.sin(phi) * Math.cos(teta)
    var z = r * Math.sin(teta)
    return [x, y, z]
}

function scalar(u, v) {
    return u[0] * v[0] + u[1] * v[1] + u[2] * v[2]
}

function norm(v) {
    return Math.sqrt(scalar(v, v))
}

function unitarize(v) {
    return [v[0] / norm(v), v[1] / norm(v), v[2] / norm(v)]
}

function p(m, a) {
    var tab = []
    for (var i = 0; i <= 2; i++) {
        tab[i] = m[i] - scalar(unitarize(a), m) * unitarize(a)[i] + norm(a) * unitarize(a)[i]
    }
    return tab
}
// console.log(p(m, a), a)

function vectorAB(a, b) {
    return [b[0] - a[0], b[1] - a[1], b[1] - a[1]]
}
// console.log(p(c, a))
//  console.log(n, l)

function vectorial(u, v) {
    return [u[1] * v[2] - u[2] * v[1], u[2] * v[0] - u[0] * v[2], u[0] * v[1] - u[1] * v[0]]
}

function findAngle(a, m) {
    m = coordcirctocart(m)
    a = coordcirctocart(a)
    var c = []
    if (a[0] > 0) {
        c = [0, 0, r]
    } else {
        c = [0, 0, -r]
    }
    var n = vectorAB(a, p(c, a))
    var l = vectorAB(a, p(m, a))
    var cosboussole = scalar(n, l) / (norm(n) * norm(l))
    var angleboussole = Math.acos(Math.trunc(cosboussole * 100000) / 100000) * 180 / pi
    // var sinangle = norm(vectorial(n, l)) / (norm(l, l) * norm(n, n))
    // console.log(scalar(n, l))
    if (scalar(vectorial(n, l), a) > 0) {
        return (360 - angleboussole) % 360
    } else {
        return (angleboussole) % 360
    }
}

function distance(a, m) {
    a = coordcirctocart(a)
    m = coordcirctocart(m)
    var moncos = scalar(a, m) / (norm(a) * norm(m))
    var angle = Math.acos(moncos) * 180 / pi
    return 2 * pi * r * angle / 360
}


var myapp = new Vue({
    el: '#myapp',
    data: {
        locs: [],

        idselected: 0,
        actloc: "",
        angle: 0,
        angleDestinationToNorth: 0,
        angleNord: 0,
        distance: null,
        compass: false,
        altitude: 0,
        speed: 0,
        trueAnglenord: 0,
        compassAnglenord: 0,
        positionhistory: [],
        maxspeed: 0,
        magneticCompassError: 0,
        newmagneticCompassError: 0,
        magneticCompassErrors: [],
        speeds: [],

        myspeed: 0,
        changelong: 0,
        changelat: 0,
        actposlat: 0,
        actposlong: 0,
    },
})

hammertime = new Hammer(document.getElementById('myapp'))
hammertime.on('swipeleft', ()=>{window.location.href = 'add.html'})
hammertime.on('swiperight', ()=>{window.location.href = 'destinations.html'})


document.addEventListener('deviceready', onDeviceReady, false);


console.log(-1)
function onDeviceReady() {
    function start_backend() {
        console.log("permission received")
        // Cordova is now initialized. Have fun!
        // var storage = window.localStorage
        // console.log('Running cordova-' + cordova.platformId + '@' + cordova.version);
        // function save() {
        //     storage.setItem('coords', JSON.stringify(tablocs.locs))
        // }
        // function load() {
        //     if (storage.getItem('coords') !== null) {
        //         tablocs.locs = JSON.parse(storage.getItem('coords'))
        //         tablocs.select(0)
        //     }
        // }


        // load()
        function orientation() {
            console.log(window.DeviceOrientationEvent, 'ontouchstart' in window)
            if (window.DeviceOrientationEvent) { // && 'ontouchstart' in window
                window.addEventListener('deviceorientationabsolute', (pos) => {
                    myapp.compassAnglenord = pos.alpha //on tourne en sens direct Nord = 0 , ouest = 90
                    myapp.compass = true;
                    myapp.trueAnglenord = myapp.compassAnglenord


                    myapp.angleNord = (360 + myapp.compassAnglenord - myapp.magneticCompassError) % 360
                    myapp.angle = (myapp.angleDestinationToNorth + myapp.compassAnglenord) % 360
                    // document.getElementById('compass').style.transform = `rotate(${myapp.angleNord}deg)`
                    // document.getElementById('myCanvas').style.transform = `rotate(${myapp.angle}deg)`
                    //myapp.angleNord = myapp.compassAnglenord
                })
            } else {
                console.error('DeviceOrientationEvent not supported')
                myapp.compass = false;
            }
        }


        function trueNorth(old, newpos, accuracy) {
            console.log(old, newpos)

            myapp.changelat = newpos.coords.latitude - old.coords.latitude
            myapp.changelong = newpos.coords.longitude - old.coords.longitude
            console.log(myapp.changelat, myapp.changelong)

            if (Math.sqrt(myapp.changelat ** 2 + myapp.changelong ** 2) > 0) {
                var rampe = (old.coords.longitude - newpos.coords.longitude) / (old.coords.latitude - newpos.coords.latitude)
                console.log('rampe', rampe)

                //console.log('ramp', newpos.coords.latitude - old.coords.latitude, newpos.coords.longitude - old.coords.longitude, rampe)
                var trueAnglenord = Math.trunc(Math.atan(rampe) * 180 / pi)
                if (trueAnglenord == NaN) {
                    trueAnglenord = 0
                }
                // if (newpos.coords.latitude > old.coords.latitude){
                //     trueAnglenord = (360+ trueAnglenord)
                // } else {
                //     trueAnglenord = (180 + trueAnglenord) % 360
                // }
                // N : 180 E : 270 S : 360 O : 440 
                trueAnglenord -= 180
                // N: 0 E: 90 S: 180 O: 270
                if (newpos.coords.latitude > old.coords.latitude) { //Based on north
                    trueAnglenord = (360 - trueAnglenord) % 360
                } else { // based on south
                    trueAnglenord = (180 - trueAnglenord) % 360
                }
                // N: 0 O: 90 S: 180 E; 270


                myapp.trueAnglenord = trueAnglenord
                var compassSnapshot = myapp.compassAnglenord // very important to take a snapshot of the compass angle due to vuejs

                myapp.newmagneticCompassError = (360 + trueAnglenord - compassSnapshot) % 360
                myapp.magneticCompassErrors.push((360 + trueAnglenord - compassSnapshot) % 360)
                myapp.magneticCompassError = myapp.newmagneticCompassError
                if( myapp.compass == false){
                    myapp.angle = (myapp.trueAnglenord + myapp.angleDestinationToNorth) % 360
                }

                // myapp.speeds.push(speed)


                // weighted_mean = 0
                // for (var i = 0; i < myapp.magneticCompassErrors.length; i++) {
                //     weighted_mean += myapp.magneticCompassErrors[i]
                // }
                // weighted_mean = weighted_mean / myapp.magneticCompassErrors.length
                // myapp.magneticCompassError = weighted_mean

                // // myapp.angleNord = (trueAnglenord)
                // myapp.speeds.push(speed)

            }

        }

        function position() {
            console.log('pos')
            if (navigator.geolocation) {
                navigator.geolocation.watchPosition((position) => {

                    // if (tablocs.idselected !== -1) {       

                    var a = [position.coords.latitude, position.coords.longitude]
                   
                    var m = [myapp.locs[myapp.idselected].coords.lat, myapp.locs[myapp.idselected].coords.long]
                    myapp.angleDestinationToNorth = Math.trunc(findAngle(a, m))
                    myapp.angle = (myapp.angleDestinationToNorth + myapp.compassAnglenord) % 360
                    myapp.distance = Math.trunc(distance(a, m))
                    myapp.altitude = position.coords.altitude
                    myapp.speed = position.coords.speed
                    // position.time = Date.now()
                    myapp.positionhistory.push(position)
                    if (myapp.positionhistory.length >= 10) {
                        myapp.positionhistory.shift()
                    }
                    var length = myapp.positionhistory.length
                    if (length >= 2) {
                        console.log(length, typeof position)
                        console.log(myapp.positionhistory[length - 1], myapp.positionhistory[length - 2])
                        trueNorth(myapp.positionhistory[length - 1], myapp.positionhistory[length - 2])
                    }


                    // tablocs.info = window.DeviceOrientationEvent && 'ontouchstart' in window

                    // compassDisc.style.transform = `rotate(${tablocs.angleNord}deg)`

                    console.log(position.coords.latitude, position.coords.longitude)
                }, (error) => { alert("We would like you to activate your geolocalisation to use this app"); window.location.href = "index.html"
            }, { enableHighAccuracy: true })
            }
        }

        myapp.locs = JSON.parse(localStorage.getItem('locations'))
        if (myapp.locs === null) {
            myapp.locs = []
        }
        myapp.idselected = JSON.parse(localStorage.getItem('target'))
        if (myapp.idselected === 0) {
            myapp.idselected = 0
        }

        console.log("memstate",myapp.locs, myapp.idselected)
        if (myapp.locs.length == 0) {
            navigator.geolocation.getCurrentPosition((pos) => {
                myapp.locs.push({ name: 'new', coords: { 'lat': pos.coords.latitude, 'long': pos.coords.longitude } })
                myapp.idselected = 0
                localStorage.setItem('locations', JSON.stringify(myapp.locs))
                localStorage.setItem('target', 0)
                console.log("memstate",myapp.locs, myapp.idselected)
                myapp.actloc = myapp.locs[myapp.idselected].name
                position()
                orientation()
            })
        } else {

            myapp.actloc = myapp.locs[myapp.idselected].name
            position()
            orientation()
        }

    }
    start_backend()
    // console.log("can request?", cordova.plugins.locationAccuracy)
    // cordova.plugins.locationAccuracy.canRequest((canRequest)=>{
    //     console.log("can request", canRequest)
    //     if (canRequest){
    //         console.log("requesting")
    //         cordova.plugins.locationAccuracy.request(()=>{
    //             console.log("requested successfully")

    //         }, (error)=>{

    //             console.error('Error requesting location accuracy', error)
    //         }, cordova.plugins.locationAccuracy.REQUEST_PRIORITY_HIGH_ACCURACY)
    //     } else {
    //         console.error('Cannot request location accuracy')
    //     }

    // })

}
