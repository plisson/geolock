
const myapp = new Vue({
    el: '#myapp',
    
    data: {
        locs : [],
        targetedIndex : -1
    },
    methods: {
        removeLocation(index) {
            this.locs.splice(index, 1);
            if (index == this.targetedIndex) {
                this.targetedIndex = -1;
            }
            localStorage.setItem('locations', JSON.stringify(this.locs));
        },
        targetLocation(index) {
            localStorage.setItem('target', index);
            this.targetedIndex = index;
        }
    }
    
    // Define the template for the Vue instance
});

var value = localStorage.getItem('locations');
var target = localStorage.getItem('target');
try {
    myapp.locs = JSON.parse(value);
    console.log(target, typeof target)
    myapp.targetedIndex = parseInt(target);
    console.log(myapp.targetedIndex)
} catch (error) {
    myapp.locs = [];
    console.log(error)
}

hammertime = new Hammer(document.getElementById('myapp'));
hammertime.on('swipeleft', ()=>{window.location.href = 'index.html'})