
var hammertime = new Hammer(document.getElementById('wrapper'));
hammertime.on('swiperight', () => { window.location.href = 'index.html' })
document.addEventListener('deviceready', () => {
    var map = L.map('map', { center: [50, 50] }).setView([45, 2.5], 6);
    L.tileLayer('https://tile.openstreetmap.org/{z}/{x}/{y}.png', {
        maxZoom: 19,
        attribution: '&copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>'
    }).addTo(map);
    var popup = L.popup();

    var newLocation = null;

    function onMapClick(e) {
        popup
            .setLatLng(e.latlng)
            .setContent("You clicked the map at " + e.latlng.toString())
            .openOn(map);
        console.log(e)
        newLocation = { 'name': document.getElementById("destinationName").value, 'coords': { 'lat': e.latlng.lat, 'long': e.latlng.lng } };
        document.getElementById("chosenlocation").innerHTML = "latitude : "+ e.latlng.lat + ", longitude : "+ e.latlng.lng
    }

    map.on('click', onMapClick);
    window.addEventListener('resize', function () {
        map.invalidateSize();
    });

    document.getElementById('submit').addEventListener('click', (event) => {
        event.preventDefault();
        locations_str = localStorage.getItem('locations');
        try {
            locations = JSON.parse(locations_str);
        } catch (error) {
            locations = [];
        }
        if (locations == null) {
            locations = [];
        }
        if (newLocation != undefined) {
            newLocation['name'] = document.getElementById("destinationName").value;
            locations.push(newLocation);
        }
        console.log(locations, JSON.stringify(locations))
        localStorage.setItem('locations', JSON.stringify(locations));
        localStorage.setItem('target', locations.length - 1);
        window.location.href = 'destinations.html';
    }, false)
    navigator.geolocation.getCurrentPosition((pos) => {
        newLocation = {name:"here", coords: {'lat': pos.coords.latitude, 'long': pos.coords.longitude}}
        document.getElementById("chosenlocation").innerText = "latitude : "+ pos.coords.latitude + ", longitude : " + pos.coords.longitude
        
        var marker = L.marker([pos.coords.latitude, pos.coords.longitude]).addTo(map);
        marker.bindPopup("<b>You are here</b>").openPopup();
    }, (error) => { console.log(error) }, { enableHighAccuracy: true })
}, false);


